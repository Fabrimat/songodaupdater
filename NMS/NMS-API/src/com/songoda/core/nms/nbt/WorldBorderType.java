package com.songoda.core.nms.nbt;

public enum WorldBorderType {
    RED, GREEN, BLUE, OFF
}
