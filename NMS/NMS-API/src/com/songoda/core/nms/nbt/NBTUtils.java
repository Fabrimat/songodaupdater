package com.songoda.core.nms.nbt;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;

public interface NBTUtils {
    void setBlockFast(Block block, Material material, byte data);
    void sendChunk(Player player, Chunk chunk);
    void sendWorldBorder(Player player, WorldBorderType type, double size, Location centerLocation);
    void sendTitle(Player player, @Nullable String title, @Nullable String subtitle, int fadeIn, int displayTime, int fadeOut);
}
