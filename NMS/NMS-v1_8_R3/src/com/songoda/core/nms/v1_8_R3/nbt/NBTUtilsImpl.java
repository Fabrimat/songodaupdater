package com.songoda.core.nms.v1_8_R3.nbt;

import com.songoda.core.nms.nbt.NBTUtils;
import com.songoda.core.nms.nbt.WorldBorderType;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

public class NBTUtilsImpl implements NBTUtils {
    
    @Override
    public void setBlockFast(Block block, org.bukkit.Material material, byte data) {
        World nmsWorld = ((CraftWorld) block.getWorld()).getHandle();
        BlockPosition bp = new BlockPosition(block.getLocation().getX(), block.getLocation().getY(), block.getLocation().getZ());
        IBlockData ibd = net.minecraft.server.v1_8_R3.Block.getByCombinedId(material.getId() + (data << 12));
        nmsWorld.setTypeAndData(bp, ibd, 2);
    }
    
    @Override
    public void sendChunk(Player player, Chunk chunk) {
        PacketPlayOutMapChunk packetPlayOutMapChunk = new PacketPlayOutMapChunk(((CraftChunk) chunk).getHandle(), true, 65535);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutMapChunk);
    }
    
    @Override
    public void sendWorldBorder(Player player, WorldBorderType type, double size, Location centerLocation) {
        WorldBorder worldBorder = new WorldBorder();
        worldBorder.world = ((CraftWorld) centerLocation.getWorld()).getHandle();
        worldBorder.setCenter(centerLocation.getBlockX() + 0.5, centerLocation.getBlockZ() + 0.5);
    
        switch (type) {
            case RED:
                worldBorder.transitionSizeBetween(size, size - 1.0D, 20000000L);
            case GREEN:
                worldBorder.transitionSizeBetween(size - 0.1D, size, 20000000L);
            case BLUE:
                worldBorder.setSize(size);
                worldBorder.setWarningDistance(0);
                worldBorder.setWarningTime(0);
                break;
            case OFF:
                worldBorder.setSize(Integer.MAX_VALUE);
                break;
        }
    
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(
                new PacketPlayOutWorldBorder(worldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE));
    }
    
    @Override
    public void sendTitle(Player player, @Nullable String title, @Nullable String subtitle, int fadeIn, int displayTime, int fadeOut) {
        IChatBaseComponent iChatBaseComponentTitle = IChatBaseComponent.ChatSerializer.a(ChatColor.translateAlternateColorCodes('&', "{\"text\":\"" + title + "\"}"));
        IChatBaseComponent iChatBaseComponentSubtitle = IChatBaseComponent.ChatSerializer.a(ChatColor.translateAlternateColorCodes('&', "{\"text\":\"" + subtitle + "\"}"));
    
        PacketPlayOutTitle packetPlayOutTitleTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, iChatBaseComponentTitle, fadeIn, displayTime, fadeOut);
        PacketPlayOutTitle packetPlayOutTitleSubtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, iChatBaseComponentSubtitle, fadeIn, displayTime, fadeOut);
    
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutTitleTitle);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutTitleSubtitle);
    }
}
